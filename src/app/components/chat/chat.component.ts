import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { User } from '../../shared/user';

@Component({
	selector: 'chat',
	templateUrl: './chat.component.pug',
	styleUrls: ['./chat.component.styl']
})
export class ChatComponent {

	constructor(private dataService: DataService,
				private router: Router) {}

}