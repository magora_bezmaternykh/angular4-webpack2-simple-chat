#Webpack2 Angular4 ES6
Template for simple chat

## Environment params

Environment (production / development) DEV - development, other value - production
```
	$ NODE_ENV
```

##Quick start

###Install dependencies
```
npm install
```
####Dev
```
npm start
```
In your browser, navigate to: http://localhost:8080/

####Production
```
npm run build 
```
Copy everything in `dist/` folder to the server.