import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../shared/user';

@Pipe({
    name: 'usersSearchFilter'
})
export class UsersSearchPipe implements PipeTransform {
    transform(users: User[], filter: string): any {
        if (!users || !filter) {
            return users;
        }

        return users.filter((user) => ~user.login.indexOf(filter));
    }
}