"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var app_component_1 = require("./app.component");
var sign_in_component_1 = require("./components/sign-in/sign-in.component");
var chat_component_1 = require("./components/chat/chat.component");
var chat_guard_1 = require("./components/chat/chat.guard");
var chat_current_user_component_1 = require("./components/chat-current-user/chat-current-user.component");
var chat_users_list_component_1 = require("./components/chat-users-list/chat-users-list.component");
var chat_users_list_pipe_1 = require("./components/chat-users-list/chat-users-list.pipe");
var chat_window_component_1 = require("./components/chat-window/chat-window.component");
var data_service_1 = require("./services/data.service");
// определение маршрутов
var appRoutes = [
    { path: '', component: sign_in_component_1.SignInComponent },
    { path: 'chat', component: chat_component_1.ChatComponent, canActivate: [chat_guard_1.ChatGuard] },
    { path: '**', redirectTo: '/' }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            router_1.RouterModule.forRoot(appRoutes)
        ],
        declarations: [
            app_component_1.AppComponent,
            sign_in_component_1.SignInComponent,
            chat_component_1.ChatComponent,
            chat_current_user_component_1.ChatCurrentUserComponent,
            chat_users_list_component_1.ChatUsersListComponent,
            chat_users_list_pipe_1.UsersSearchPipe,
            chat_window_component_1.ChatWindowComponent
        ],
        providers: [
            data_service_1.DataService,
            chat_guard_1.ChatGuard
        ],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map