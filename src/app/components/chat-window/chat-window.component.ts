import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Msg } from '../../shared/message';

@Component({
	selector: 'chat-window',
	templateUrl: './chat-window.component.pug',
	styleUrls: ['./chat-window.component.styl']
})
export class ChatWindowComponent implements OnInit {

	message: string = '';
	chat: Msg[];
	currentUserName: string = '';

	constructor(private dataService: DataService) {
		// observable watcher
		this.dataService.currentChatChanged().subscribe({
			next: (chat) => this.chat = chat
		});
	}

	ngOnInit() {
		this.chat = this.dataService.currentChat;
		this.currentUserName = this.dataService.currentUser.login;
	}

	onKeydown(event: any) {
		if (event.keyCode === 13 && !event.shiftKey) this.sendMsg();
	}

	sendMsg() {
		this.dataService.addMsg(this.message);
		this.message = '';
	}
}