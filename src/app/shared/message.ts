export class Msg {
	constructor(public userName: string,
		public text: string) {}
}