import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { User } from '../shared/user';
import { Msg } from '../shared/message';

@Injectable()
export class DataService {

	currentChat: Msg[];
	// observable source
	currentChatSubject: Subject<Msg[]> = new Subject();

	partner: string;
	chats: object;


	constructor() {
		this.nativeWindow.addEventListener('storage', (event: any) => {
			if (event.key === 'a2-chats' && this.partner) {
				this.setCurrentChat(this.partner);
			}
		});
	}

	get nativeWindow(): any {
		return window;
	}

	// observable stream
	currentChatChanged(): Subject<Msg[]> {
		return this.currentChatSubject;
	}

	get users(): object {
		return JSON.parse(window.localStorage.getItem('a2-users') || null) || {};
	}

	get usersArray(): User[] {
		return Object.keys(this.users).map((login) => new User(login, this.users[login]));
	}

	addUser(user: User): void {
		let users = this.users;
		users[user.login] = user.password;
		window.localStorage.setItem('a2-users', JSON.stringify(users));
	}

	setCurrentUser(user: User) {
		window.localStorage.setItem('a2-user', JSON.stringify(user));
	}

	logOut() {
		this.removeCurrentUser();
		this.currentChat = null;
		this.partner = null;
	}

	removeCurrentUser() {
		window.localStorage.removeItem('a2-user');
	}

	get currentUser(): User {
		return JSON.parse(window.localStorage.getItem('a2-user') || null);
	}

	regValidation(login: string): boolean {
		return !this.users[login];
	}

	authValidation(user: User) {
		let isLoginValid = !!this.users[user.login];
		let isPasswordValid = this.users[user.login] === user.password;
		return { isLoginValid, isPasswordValid };
	}

	currentUserValidation() {
		return window.localStorage.getItem('a2-user');
	}

	getChats(): object {
		return JSON.parse(window.localStorage.getItem('a2-chats')) || {};
	}

	setChats(): void {
		window.localStorage.setItem('a2-chats', JSON.stringify(this.chats));
	}

	setCurrentChat(userLogin: string): void {
		this.chats = this.getChats();
		this.partner = userLogin;
		const chatId = `${this.currentUser.login}${userLogin}`;
		const chatIdReverse = `${userLogin}${this.currentUser.login}`;
		let chat = this.chats[chatId] || this.chats[chatIdReverse];
		if (!chat) {
			this.chats[chatId] = [];
			chat = this.chats[chatId];
		}

		this.currentChat = chat;
		// observable command
		this.currentChatSubject.next(this.currentChat);
	}

	addMsg(msgText: string): void {
		if (msgText) {
			let message = new Msg(this.currentUser.login, msgText);
			this.currentChat.push(message);
			// observable command
			this.currentChatSubject.next(this.currentChat);
			this.setChats();
		}
	}

}