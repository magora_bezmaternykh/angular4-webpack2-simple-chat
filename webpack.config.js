var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');

const NODE_ENV = process.env.NODE_ENV || 'DEV';
const PUBLIC_PATH = (NODE_ENV === 'DEV')
	? '/'
	: path.join(__dirname, 'dist');

console.log('process.env.NODE_ENV ==> ', process.env.NODE_ENV);
console.log('PUBLIC_PATH ==> ', PUBLIC_PATH);

module.exports = {
	entry: {
		'polyfills': './src/polyfills.ts',
		'vendor': './src/vendor.ts',
		'app': './src/app/main.ts'
	},
	output: {
		path: path.resolve('dist'),
		publicPath: PUBLIC_PATH,
		filename: '[name].[hash].js'
	},
	resolve: {
		extensions: ['.ts', '.js']
	},

	module: {
		rules: [
			{
				test: /\.ts$/,
				loaders: [{
					loader: 'awesome-typescript-loader',
					options: { configFileName: path.resolve('tsconfig.json') }
				} , 'angular2-template-loader']
			},
			{
				test: /\.html$/,
				loader: 'html-loader'
			},
			{
				test: /\.(pug|jade)$/,
				use: ['raw-loader', 'pug-html-loader']
			},
			{
				test: /\.(jpe?g|png|gif)$/,
				exclude: /(node_modules)/,
				loader: 'url-loader'
			},
			{
				test: /\.styl$/,
				exclude: path.resolve('src', 'app'),
				loader: ExtractTextPlugin.extract({ fallbackLoader: 'style-loader', loader: 'css-loader!stylus-loader' })
			},
			{
				test: /\.styl$/,
				loader: 'raw-loader!stylus-loader',
				include: path.resolve('src', 'app'),
				exclude: /node_modules/
			},
			{
				test: /\.css$/,
				exclude: path.resolve('src', 'app'),
				loader: ExtractTextPlugin.extract({ fallbackLoader: 'style-loader', loader: 'css-loader?sourceMap' })
			},
			{
				test: /\.css$/,
				include: path.resolve('src', 'app'),
				loader: 'raw-loader'
			}
		]
	},

	plugins: [
		new webpack.ContextReplacementPlugin(
			/angular(\|\/)core(\|\/)(esm(\|\/)src|src)(\|\/)linker/,
			path.resolve('./src'),
			{}
		),

		new webpack.optimize.CommonsChunkPlugin({
			name: ['app', 'vendor', 'polyfills']
		}),

		new HtmlWebpackPlugin({
			template: 'src/index.pug'
		}),

		new ExtractTextPlugin('[name].[hash].css'),

		new webpack.NoEmitOnErrorsPlugin(),

		new webpack.optimize.UglifyJsPlugin({
			mangle: {
				keep_fnames: true
			}
		}),

		new webpack.LoaderOptionsPlugin({
			htmlLoader: {
				minimize: false
			}
		})
	]
};