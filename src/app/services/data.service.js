"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var user_1 = require("../shared/user");
var message_1 = require("../shared/message");
var DataService = (function () {
    function DataService() {
        var _this = this;
        // observable source
        this.currentChatSubject = new Subject_1.Subject();
        this.nativeWindow.addEventListener('storage', function (event) {
            if (event.key === 'a2-chats' && _this.partner) {
                _this.setCurrentChat(_this.partner);
            }
        });
    }
    Object.defineProperty(DataService.prototype, "nativeWindow", {
        get: function () {
            return window;
        },
        enumerable: true,
        configurable: true
    });
    // observable stream
    DataService.prototype.currentChatChanged = function () {
        return this.currentChatSubject;
    };
    Object.defineProperty(DataService.prototype, "users", {
        get: function () {
            return JSON.parse(window.localStorage.getItem('a2-users') || null) || {};
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DataService.prototype, "usersArray", {
        get: function () {
            var _this = this;
            return Object.keys(this.users).map(function (login) { return new user_1.User(login, _this.users[login]); });
        },
        enumerable: true,
        configurable: true
    });
    DataService.prototype.addUser = function (user) {
        var users = this.users;
        users[user.login] = user.password;
        window.localStorage.setItem('a2-users', JSON.stringify(users));
    };
    DataService.prototype.setCurrentUser = function (user) {
        window.localStorage.setItem('a2-user', JSON.stringify(user));
    };
    DataService.prototype.logOut = function () {
        this.removeCurrentUser();
        this.currentChat = null;
        this.partner = null;
    };
    DataService.prototype.removeCurrentUser = function () {
        window.localStorage.removeItem('a2-user');
    };
    Object.defineProperty(DataService.prototype, "currentUser", {
        get: function () {
            return JSON.parse(window.localStorage.getItem('a2-user') || null);
        },
        enumerable: true,
        configurable: true
    });
    DataService.prototype.regValidation = function (login) {
        return !this.users[login];
    };
    DataService.prototype.authValidation = function (user) {
        var isLoginValid = !!this.users[user.login];
        var isPasswordValid = this.users[user.login] === user.password;
        return { isLoginValid: isLoginValid, isPasswordValid: isPasswordValid };
    };
    DataService.prototype.currentUserValidation = function () {
        return window.localStorage.getItem('a2-user');
    };
    DataService.prototype.getChats = function () {
        return JSON.parse(window.localStorage.getItem('a2-chats')) || {};
    };
    DataService.prototype.setChats = function () {
        window.localStorage.setItem('a2-chats', JSON.stringify(this.chats));
    };
    DataService.prototype.setCurrentChat = function (userLogin) {
        this.chats = this.getChats();
        this.partner = userLogin;
        var chatId = "" + this.currentUser.login + userLogin;
        var chatIdReverse = "" + userLogin + this.currentUser.login;
        var chat = this.chats[chatId] || this.chats[chatIdReverse];
        if (!chat) {
            this.chats[chatId] = [];
            chat = this.chats[chatId];
        }
        this.currentChat = chat;
        // observable command
        this.currentChatSubject.next(this.currentChat);
    };
    DataService.prototype.addMsg = function (msgText) {
        if (msgText) {
            var message = new message_1.Msg(this.currentUser.login, msgText);
            this.currentChat.push(message);
            // observable command
            this.currentChatSubject.next(this.currentChat);
            this.setChats();
        }
    };
    return DataService;
}());
DataService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], DataService);
exports.DataService = DataService;
//# sourceMappingURL=data.service.js.map