import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Rx";
import { DataService } from '../../services/data.service';

@Injectable()
export class ChatGuard implements CanActivate {

    constructor(private dataService: DataService,
                private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> | boolean {
        if (this.dataService.currentUserValidation()) {
            return true;
        } else {
            this.router.navigate(['']);
            return false;
        }
    }

}