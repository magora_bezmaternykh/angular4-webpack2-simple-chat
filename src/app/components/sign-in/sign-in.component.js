"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var data_service_1 = require("../../services/data.service");
var user_1 = require("../../shared/user");
var SignInComponent = (function () {
    function SignInComponent(dataService, router) {
        this.dataService = dataService;
        this.router = router;
        this.login = '';
        this.password = '';
        // values for validation
        this.isLoginValid = true;
        this.isPasswordValid = true;
        this.isNotLoginDublicate = true;
    }
    Object.defineProperty(SignInComponent.prototype, "isCredsEmpty", {
        get: function () {
            return !this.login || !this.password;
        },
        enumerable: true,
        configurable: true
    });
    SignInComponent.prototype.registration = function () {
        this.isLoginValid = true;
        this.isPasswordValid = true;
        this.isNotLoginDublicate = this.dataService.regValidation(this.login);
        if (this.isNotLoginDublicate) {
            var user = new user_1.User(this.login, this.password);
            this.dataService.addUser(user);
            this.dataService.setCurrentUser(user);
            this.router.navigate(['chat']);
        }
    };
    SignInComponent.prototype.authorization = function () {
        this.isNotLoginDublicate = true;
        var user = new user_1.User(this.login, this.password);
        var validationObj = this.dataService.authValidation(user);
        this.isLoginValid = validationObj.isLoginValid;
        this.isPasswordValid = validationObj.isPasswordValid;
        if (this.isLoginValid && this.isPasswordValid) {
            this.dataService.setCurrentUser(user);
            this.router.navigate(['chat']);
        }
    };
    return SignInComponent;
}());
SignInComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'sign-in',
        templateUrl: 'sign-in.component.html',
        styleUrls: ['sign-in.component.css']
    }),
    __metadata("design:paramtypes", [data_service_1.DataService,
        router_1.Router])
], SignInComponent);
exports.SignInComponent = SignInComponent;
//# sourceMappingURL=sign-in.component.js.map