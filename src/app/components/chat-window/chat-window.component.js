"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var data_service_1 = require("../../services/data.service");
var ChatWindowComponent = (function () {
    function ChatWindowComponent(dataService) {
        var _this = this;
        this.dataService = dataService;
        this.message = '';
        this.currentUserName = '';
        // observable watcher
        this.dataService.currentChatChanged().subscribe({
            next: function (chat) { return _this.chat = chat; }
        });
    }
    ChatWindowComponent.prototype.ngOnInit = function () {
        this.chat = this.dataService.currentChat;
        this.currentUserName = this.dataService.currentUser.login;
    };
    ChatWindowComponent.prototype.onKeydown = function (event) {
        if (event.keyCode === 13 && !event.shiftKey)
            this.sendMsg();
    };
    ChatWindowComponent.prototype.sendMsg = function () {
        this.dataService.addMsg(this.message);
        this.message = '';
    };
    return ChatWindowComponent;
}());
ChatWindowComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'chat-window',
        templateUrl: 'chat-window.component.html',
        styleUrls: ['chat-window.component.css']
    }),
    __metadata("design:paramtypes", [data_service_1.DataService])
], ChatWindowComponent);
exports.ChatWindowComponent = ChatWindowComponent;
//# sourceMappingURL=chat-window.component.js.map