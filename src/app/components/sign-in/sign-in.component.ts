import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { User } from '../../shared/user';

@Component({
	selector: 'sign-in',
	templateUrl: './sign-in.component.pug',
	styleUrls: ['./sign-in.component.styl']
})
export class SignInComponent {
	login: string = '';
	password: string = '';

	// values for validation
	isLoginValid: boolean = true;
	isPasswordValid: boolean = true;
	isNotLoginDublicate: boolean = true;

	constructor(private dataService: DataService,
							private router: Router) {}

	get isCredsEmpty() {
		return !this.login || !this.password;
	}

	registration() {
		this.isLoginValid = true;
		this.isPasswordValid = true;

		this.isNotLoginDublicate = this.dataService.regValidation(this.login);
		if (this.isNotLoginDublicate) {
			let user = new User(this.login, this.password);
			this.dataService.addUser(user);

			this.dataService.setCurrentUser(user);
			this.router.navigate(['chat']);
		}
	}

	authorization() {
		this.isNotLoginDublicate = true;

		let user = new User(this.login, this.password);
		let validationObj = this.dataService.authValidation(user);
		this.isLoginValid = validationObj.isLoginValid;
		this.isPasswordValid = validationObj.isPasswordValid;
		if (this.isLoginValid && this.isPasswordValid) {
			this.dataService.setCurrentUser(user);
			this.router.navigate(['chat']);
		}
	}
}
