import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
	selector: 'chat-current-user',
	templateUrl: './chat-current-user.component.pug',
	styleUrls: ['./chat-current-user.component.styl']
})
export class ChatCurrentUserComponent {

	userName: string = '';

	constructor(private dataService: DataService,
								private router: Router) {}

	ngOnInit() {
		this.userName = this.dataService.currentUser.login;
	}

	logOut() {
		this.dataService.logOut();
		this.router.navigate(['']);
	}

}