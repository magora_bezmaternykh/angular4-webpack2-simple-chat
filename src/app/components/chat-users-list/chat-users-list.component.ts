import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { User } from '../../shared/user';

@Component({
	selector: 'chat-users-list',
	templateUrl: './chat-users-list.component.pug',
	styleUrls: ['./chat-users-list.component.styl']
})
export class ChatUsersListComponent implements OnInit  {

	users: User[] = [];
	currentUser: User;
	chatPartnerLogin: string = '';

	constructor(private dataService: DataService) {}

	ngOnInit() {
		this.users = this.dataService.usersArray;
		this.currentUser = this.dataService.currentUser;
	}

	chooseChat(userLogin: string) {
		this.chatPartnerLogin = userLogin;
		this.dataService.setCurrentChat(userLogin);
	}

}