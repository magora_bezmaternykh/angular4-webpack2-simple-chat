import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { ChatComponent } from './components/chat/chat.component';
import { ChatGuard } from './components/chat/chat.guard';
import { ChatCurrentUserComponent } from './components/chat-current-user/chat-current-user.component';
import { ChatUsersListComponent } from './components/chat-users-list/chat-users-list.component';
import { UsersSearchPipe } from './components/chat-users-list/chat-users-list.pipe';
import { ChatWindowComponent } from './components/chat-window/chat-window.component';

import { DataService } from './services/data.service';

// определение маршрутов
const appRoutes: Routes = [
	{ path: '', component: SignInComponent },
	{ path: 'chat', component: ChatComponent, canActivate: [ChatGuard] },
	{ path: '**', redirectTo: '/' }
];

@NgModule({
	imports: [
		BrowserModule,
		FormsModule,
		RouterModule.forRoot(appRoutes)
	],
	declarations: [
		AppComponent,
		SignInComponent,
		ChatComponent,
		ChatCurrentUserComponent,
		ChatUsersListComponent,
		UsersSearchPipe,
		ChatWindowComponent
	],
	providers: [
		DataService,
		ChatGuard
	],
	bootstrap: [AppComponent]
})
export class AppModule {

}