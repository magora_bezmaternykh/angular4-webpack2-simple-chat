"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Msg = (function () {
    function Msg(userName, text) {
        this.userName = userName;
        this.text = text;
    }
    return Msg;
}());
exports.Msg = Msg;
//# sourceMappingURL=message.js.map